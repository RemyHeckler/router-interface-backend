import {
  getNetworkSettings,
  getWifiNetworks,
  saveNetworkSettings,
} from './resolvers';

export default {
  Query: {
    networkSettings: getNetworkSettings,
    wifiNetworks: getWifiNetworks,
  },
  Mutation: {
    saveNetworkSettings,
  },
};
