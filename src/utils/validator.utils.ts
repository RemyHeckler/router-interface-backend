const IpPattern = /^(?:(?:2[0-4]\d|25[0-5]|1\d{2}|[1-9]?\d)\.){3}(?:2[0-4]\d|25[0-5]|1\d{2}|[1-9]?\d)$/;

const IpValidate = (data: any) => {
  const { ip, netMask, gateWay, preferredDns, alternativeDns } = data;
  if (ip && !IpPattern.test(ip)){
    throw new Error("Input ip must contain a correct IP");
  }
  if (netMask && !IpPattern.test(netMask)){
    throw new Error("Input net mask must contain a correct IP");
  }
  if (gateWay && !IpPattern.test(gateWay)){
    throw new Error("Input gateway must contain a correct IP");
  }
  if (preferredDns && !IpPattern.test(preferredDns)){
    throw new Error("Input preferred dns must contain a correct IP");
  }
  if (alternativeDns && !IpPattern.test(alternativeDns)){
    throw new Error("Input alternative dns must contain a correct IP");
  }
  return null
};

const ethernetValidator = (data: any) => {
  const { isAutoIp, ip, netMask, isAutoDns, preferredDns } = data;
  if (!isAutoIp && !ip) {
    throw new Error("Field ip is required");
  }
  if (!isAutoIp && !netMask) {
    throw new Error("Field net mask ip is required");
  }
  if (!isAutoDns && !preferredDns) {
    throw new Error("Field preferred dns ip is required");
  }
  return IpValidate(data);
};

const wirelessValidator = (data: any) => {
  const { isEnabled, networkName, isSecured, securityKey } = data;
  if (isEnabled && !networkName) {
    throw new Error("Field network name ip is required");;
  }
  if (isEnabled && isSecured && !securityKey) {
    throw new Error("Field security key ip is required");;
  }
  if (isEnabled) {
    return ethernetValidator(data)
  }
  return null;
};

export const validate = (data: any) => {
  ethernetValidator(data.settings.ethernet)
  wirelessValidator(data.settings.wireless)
}
