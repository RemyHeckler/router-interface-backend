import * as mongoose from 'mongoose';

const options = { keepAlive: 30000, connectTimeoutMS: 30000 };

export const openConnection = () => mongoose.connect(process.env.MONGO_URL as string, options);
export const closeConnection = () => mongoose.connection.close();
