import {
  Ethernet,
  InputEthernet,
  InputNetworkSettings,
  InputWireless,
  Mutation,
  NetworkSettings,
  Query,
  WifiNetwork,
  Wireless,
} from './schemas';

export default [
  Ethernet,
  InputEthernet,
  InputNetworkSettings,
  InputWireless,
  Mutation,
  NetworkSettings,
  Query,
  WifiNetwork,
  Wireless,
];
