import NetworkSettings from '../models/network-settings.model';
import { closeConnection, openConnection, validate } from '../utils';

export const getNetworkSettings = async () => {
  await openConnection();
  const networkSettings = await NetworkSettings.findOne({});
  await closeConnection();
  return networkSettings;
};

export const saveNetworkSettings = async (obj: any, args: any) => {
  validate(args)
  await openConnection();
  const querySettings = { upsert: true, new: true };
  const updatedSettings = await NetworkSettings.findOneAndUpdate({}, args.settings, querySettings).exec();
  await closeConnection();
  return updatedSettings;
};
