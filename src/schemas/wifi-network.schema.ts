export const WifiNetwork = `
type WifiNetwork {
  favorite: Boolean
  name: String
  strength: Float
  security: [String]
}`;
