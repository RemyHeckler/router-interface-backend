export const Query = `type Query {
  wifiNetworks: [WifiNetwork]
  networkSettings: NetworkSettings
}`;
