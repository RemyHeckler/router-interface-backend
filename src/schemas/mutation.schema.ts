export const Mutation = `
type Mutation {
  saveNetworkSettings (settings: InputNetworkSettings): NetworkSettings
}`;
