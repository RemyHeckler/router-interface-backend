export const Ethernet = `
type Ethernet {
  isAutoIp: Boolean
  ip: String
  netMask: String
  gateWay: String
  isAutoDns: Boolean
  preferredDns: String
  alternativeDns: String
}
`;

export const InputEthernet = `
input InputEthernet {
  isAutoIp: Boolean
  ip: String
  netMask: String
  gateWay: String
  isAutoDns: Boolean
  preferredDns: String
  alternativeDns: String
}
`;

export const Wireless = `
type Wireless {
  isEnabled: Boolean
  networkName: String
  isSecured: Boolean
  securityKey: String
  isAutoIp: Boolean
  ip: String
  netMask: String
  gateWay: String
  isAutoDns: Boolean
  preferredDns: String
  alternativeDns: String
}
`;

export const InputWireless = `
input InputWireless {
  isEnabled: Boolean
  networkName: String
  isSecured: Boolean
  securityKey: String
  isAutoIp: Boolean
  ip: String
  netMask: String
  gateWay: String
  isAutoDns: Boolean
  preferredDns: String
  alternativeDns: String
}
`;

export const NetworkSettings = `
type NetworkSettings {
  id: String
  ethernet: Ethernet
  wireless: Wireless
}`;

export const InputNetworkSettings = `
input InputNetworkSettings {
  ethernet: InputEthernet
  wireless: InputWireless
}`;
