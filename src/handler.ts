import '../load-config.js';

import { graphiqlLambda, graphqlLambda } from 'apollo-server-lambda';
import { Callback, Context, Handler } from 'aws-lambda';
import { makeExecutableSchema } from 'graphql-tools';
import schema from './schema';
import resolvers from './setup-resolvers';

const myGraphQLSchema = makeExecutableSchema({
  resolvers,
  typeDefs: schema,
  logger: console,
});

export const graphqlHandler: Handler = (event: any, context: Context, callback: Callback) => {
  const corsCallback = (error: any, output: any) => {
    if (output && output.headers) {
      output.headers['Access-Control-Allow-Origin'] = '*';
    }
    callback(error, output);
  };

  const handler = graphqlLambda(() => ({
    schema: myGraphQLSchema,
    context: { event, context },
  }));

  return handler(event, context, corsCallback);
};

export const graphiqlHandler = graphiqlLambda({ endpointURL: `/${process.env.GRAPHQL_URL}` });
