import { model, models, Schema } from 'mongoose';

const networkStatistic = new Schema({
  ethernet: {
    isAutoIp: { type: Boolean },
    ip: { type: String },
    netMask: { type: String },
    gateWay: { type: String },
    isAutoDns: { type: Boolean },
    preferredDns: { type: String },
    alternativeDns: { type: String },
  },
  wireless: {
    isEnabled: { type: Boolean },
    networkName: { type: String },
    isSecured: { type: Boolean },
    securityKey: { type: String },
    isAutoIp: { type: Boolean },
    ip: { type: String },
    netMask: { type: String },
    gateWay: { type: String },
    isAutoDns: { type: Boolean },
    preferredDns: { type: String },
    alternativeDns: { type: String },
  },
});

export default models.networkStatistic ? models.networkStatistic : model('networkStatistic', networkStatistic);
